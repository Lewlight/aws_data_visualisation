let AWS = require("aws-sdk");
let moment = require("moment");
let fs = require("fs");

AWS.config.update({ region: 'us-east-1' });
let documentClient = new AWS.DynamoDB.DocumentClient();

const putParamsStock = {
    TableName: "PredictedStockData",
};

let companies = ["TSLA", "HMC", "TM", "NIO", "F"]

// This function creates the json files to be used for training/testing each model.
// It then returns the data used for predictions.
async function getDataTrain(test_size) {
    let result = {};
    for (let company of companies) {
        let stockQueryParam = {
            Limit: test_size,
            TableName: "StockData",
            ExpressionAttributeValues: {
                ":symb": company,
            },
            KeyConditionExpression: "Symbol = :symb",
            ScanIndexForward: false,
        };
        try {
            // We first get the latest (test_size) points from the database.
            let responseData = await documentClient.query(stockQueryParam).promise();
            responseData.Items = responseData.Items.reverse();
            console.log(responseData.Items.slice(0, 5));

            // We create the arrays of the 4 data values for the stock data.
            let targopen = [];
            let targclose = [];
            let targhigh = [];
            let targlow = [];

            // We the first timestamp serves as the "start" date.
            let momentObject = new moment(responseData.Items[0].Timestamp);
            let startTime = momentObject.format("YYYY-MM-DD HH:mm:ss");

            // We populate the "target" arrays.
            for (let item of responseData.Items) {
                targopen.push(item["OpenPrice"]);
                targclose.push(item["ClosePrice"]);
                targhigh.push(item["High"]);
                targlow.push(item["Low"]);
            }

            // These will serve as the json objects for testing.
            let openobj = {
                start: startTime,
                cat: 0,
                target: targopen
            }
            let closeobj = {
                start: startTime,
                cat: 1,
                target: targclose
            }
            let highobj = {
                start: startTime,
                cat: 2,
                target: targhigh
            }
            let lowobj = {
                start: startTime,
                cat: 3,
                target: targlow
            }

            // We create write the json lines into a file.
            let text = JSON.stringify(openobj) + "\n" + JSON.stringify(closeobj) + "\n" + JSON.stringify(highobj) + "\n" + JSON.stringify(lowobj);
            fs.writeFile('./ML/stockPredictor/stock_test_' + company + '.json', text, 'utf8', (err) => { });

            // The "start" value is the same as the testing data.
            let trainopenobj = {
                start: startTime,
                cat: 0,
            }
            let traincloseobj = {
                start: startTime,
                cat: 1,
            }
            let trainhighobj = {
                start: startTime,
                cat: 2,
            }
            let trainlowobj = {
                start: startTime,
                cat: 3,
            }

            // We cut the data using 75% of it for training.
            trainopenobj.target = openobj.target.slice(0, Math.round(openobj.target.length * 4 / 5));
            traincloseobj.target = closeobj.target.slice(0, Math.round(closeobj.target.length * 4 / 5));
            trainhighobj.target = highobj.target.slice(0, Math.round(highobj.target.length * 4 / 5));
            trainlowobj.target = lowobj.target.slice(0, Math.round(lowobj.target.length * 4 / 5));
            
            // We write the data into a json file.
            text = JSON.stringify(trainopenobj) + "\n" + JSON.stringify(traincloseobj) + "\n" + JSON.stringify(trainhighobj) + "\n" + JSON.stringify(trainlowobj);
            fs.writeFile('./ML/stockPredictor/stock_train_' + company + '.json', text, 'utf8', (err) => { });

            // We then find the "start" date for the input data for predictions
            let movedTime = new moment(responseData.Items[lowobj.target.length - 100].Timestamp).format("YYYY-MM-DD HH:mm:ss");
            console.log(movedTime + " and the orig: " + startTime);

            // We now only keep the last 100 points from the testing data.
            openobj.target = openobj.target.slice(openobj.target.length - 100);
            closeobj.target = closeobj.target.slice(closeobj.target.length - 100);
            highobj.target = highobj.target.slice(highobj.target.length - 100);
            lowobj.target = lowobj.target.slice(lowobj.target.length - 100);
            openobj.start = movedTime;
            closeobj.start = movedTime;
            highobj.start = movedTime;
            lowobj.start = movedTime;

            // We now find the date of the latest point in the stock data.
            // This will be used to create the dates for the predictions made by SageMaker.
            movedTime = new moment(responseData.Items[responseData.Items.length - 1].Timestamp).format("YYYY-MM-DD HH:mm:ss");
            // We return the object to send to sagemaker for predictions.
            result[company] = {
                data: {
                    "instances": [openobj, closeobj, highobj, lowobj],
                    "configuration": {
                        "num_samples": 50,
                        "output_types": ["mean", "quantiles", "samples"],
                        "quantiles": ["0.1", "0.9"]
                    }
                },
                last_time: movedTime,
            };
        }
        catch (err) {
            console.log("Found some error " + JSON.stringify(err));
            console.log(err);
        }
    }
    //console.log(JSON.stringify(result));
    return result;
}

// This function is used to get the predictions before sending them to DynamoDB.
async function getFutureData() {
    // We get the data used for predictions.
    let input = await getDataTrain(1000);
    let awsRuntime = new AWS.SageMakerRuntime({});
    for (let company of companies) {
        let singleInput = input[company].data;
        let last_time = input[company].last_time;
        let endPoint = "pricePredEndpoint" + company;
        // The parameters to use sage maker.
        let params = {
            EndpointName: endPoint,
            Body: JSON.stringify(singleInput),
            ContentType: "application/json",
            Accept: "application/json"
        };
        // We create the moment representing the date for the last point of predictions.
        // This is will be used to create the dates for the predictions.
        let time = new moment(last_time);
        try {
            // We invoke the endpoint.
            let result = await awsRuntime.invokeEndpoint(params).promise();
            let responseData = JSON.parse(Buffer.from(result.Body).toString('utf-8'));
            let dataPoints = [];
            console.log("the length of prediceted data " + responseData.predictions[0].mean.length);
            // We create the data points from the response to send to the database.
            // We add one hour for each data point.
            for (let index = 0; index < 50; index++) {
                time.add(1, "hour");
                // This will be a row in the predictions table.
                let point = {
                    Symbol: company,
                    Timestamp: time.valueOf(),
                    OpenPrice: responseData.predictions[0].mean[index],
                    ClosePrice: responseData.predictions[1].mean[index],
                    High: responseData.predictions[2].mean[index],
                    Low: responseData.predictions[3].mean[index],
                }
                console.log(JSON.stringify(point));
                dataPoints.push(point);
            }
            // send the rows to the database.
            console.log("Sending data for " + company + " to prediced stock data");
            sendDataDB(dataPoints);
        }
        catch (err) {
            console.log("An error was encountered" + JSON.stringify(err));
            console.log(err);
        }

    }
}

// This function sends a list of rows into the table.
function sendDataDB(dataList) {
    for (let point of dataList) {
        putParamsStock.Item = point;
        documentClient.put(putParamsStock, (err, data) => {
            if (err) {
                console.log("Some error occured here it is: " + JSON.stringify(err));
                console.log("The data attempted to be sent " + JSON.stringify(data));
            }
            else {
                console.log("added succesfully the data");
            }
        });
    }
}

getFutureData();