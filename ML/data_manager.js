let moment = require("moment");
let fs = require("fs");


// This method takes the synthetic data and returns the chunk of data for training.
// We copy the results in a json file for training.
function separate(data) {
    data.target = data.target.slice(0, Math.round(data.target.length * 4 / 5));
    console.log(JSON.stringify(data));
    return data;
}

// This method takes the synthetic data and returns the last (number) points used for prediction.
function getDataForPred(data_path, number) {
    let data = JSON.parse(fs.readFileSync(data_path));
    // We slice the data.
    let cutDataSize = data.target.length - number
    data.target = data.target.slice(cutDataSize);
    console.log("cut data size: " + cutDataSize);
    // We move the data by the same number of hours to get the new "start" date.
    let mom = new moment(data.start);
    let advancedMom = mom.add(cutDataSize, "hours");
    data.start = advancedMom.format("YYYY-MM-DD HH:mm:ss");
    console.log(JSON.stringify(data));
    return data;
}

getDataForPred('./ML/synthetic/synthetic_test.json', 100);

