
let fs = require("fs");
let moment = require("moment");

// This script takes the synthetic data and its predictions to create traces to be displayed.
// The prdictions were copied from the response given back to the call of the api using Postman.

let mydata = JSON.parse(fs.readFileSync('./ML/synthetic/predictions.json'));

// We get the predictions to be displayed.
let mean = mydata.predictions[0].mean;
let nineQuant = mydata.predictions[0].quantiles["0.9"];
let oneQuant = mydata.predictions[0].quantiles["0.1"];
let request = JSON.parse(fs.readFileSync('./ML/synthetic/synthetic_test.json'));
let inputData = request.target;
let startPoint = request.start;

// We use moment to create the dates for each data point.
let momentHandler = new moment(startPoint);
let times = [momentHandler.format("MM-DD HH")];

// We first add one hour for each point in the synthetic data.
for(let extraHour = 1; extraHour < inputData.length; extraHour++){
    let time = momentHandler.add(1, "hour");
    times.push(time.format("MM-DD HH"));
}

// We add then the time labels for the predicted data.
let predictedTimes = []
for(let extraHour = 0; extraHour < mean.length; extraHour++){
    let time = momentHandler.add(1, "hour");
    predictedTimes.push(time.format("MM-DD HH"));
}

// Then we create the array of traces to be used by plotly.
let traces = [
    {y: inputData, x: times, name:"synthetic data"},
    {y: mean, x: predictedTimes, name:"mean"},
    {y: nineQuant, x: predictedTimes, name:"0.9 quantile"},
    {y: oneQuant, x: predictedTimes, name:"0.1 quantile"}
];
console.log(traces);

// We write the traces into a json file.
fs.writeFile('./ML/synthetic/plottedData.json', JSON.stringify(traces), 'utf8', (err) => {});
// To use the data in a client we copy it to a js file by passing it into a variable, and then 
// loading the script file to the html so the variable can be passed to plotly.
// This can be found in plottedData.js