import { stockDataReceiver, textDataReceiver } from "./Data_Receiver";
import { dataPoint, textPoint, sendListStockData, sendListTextData } from "./Data_Sender";


// These are parameters that is used to query the webservices.
const STOCK_SYMBOLS = ["TSLA", "F", "NIO", "TM", "HMC"];
const COMPANY_NAMES = ["Tesla", "Ford", "Nio", "Toyota", "Honda"];
const WINDOW_SIZE = "60min";
const DATA_KEY = "Time Series (" + WINDOW_SIZE + ")";


// This function downloads and sends tweets to the database.
async function downloadTextDataThenSend() {
    let promises: Array<Promise<object>> = [];
    for (let symbol of COMPANY_NAMES) {
        // We search for 250 tweets per company.
        promises.push(textDataReceiver(symbol, 250));
    }
    let listPoints: Array<textPoint> = [];
    try {
        // We await for the promises to resolve.
        let values = await Promise.all(promises);
        for (let index: number = 0; index < values.length; index++) {
            let name: string = STOCK_SYMBOLS[index];
            let dataValues = values[index]["statuses"];
            // we create the texPoint to be sent to the table.
            for (let tweet of dataValues) {
                let point: textPoint = {
                    ID: tweet["id"],
                    Text: tweet["text"],
                    Company: name,
                    Timestamp: new Date(tweet["created_at"]).valueOf(),
                };
                // Populate the array of points.
                listPoints.push(point);
            }
        }
    }
    catch (err) {
        console.log("Some problem occured while waiting for the data from the API " + JSON.stringify(err));
    }
    // Send the points to the database.
    sendListTextData(listPoints);
}

// This function downloads the stock data before sending it to the database.
// Works in the same way as the text downloading function.
async function downloadStockDataThenSend() {
    let promises: Array<Promise<object>> = [];
    for (let symbol of STOCK_SYMBOLS) {
        promises.push(stockDataReceiver(symbol, WINDOW_SIZE));
    }
    let listPoints: Array<dataPoint> = [];
    try {
        let values = await Promise.all(promises);
        for (let index: number = 0; index < values.length; index++) {
            let stockSymbol: string = STOCK_SYMBOLS[index];
            let dataValues = values[index]["data"][DATA_KEY];

            for (let date of Object.keys(dataValues)) {
                let point: dataPoint = {
                    Timestamp: new Date(date).valueOf(),
                    Symbol: stockSymbol,
                    OpenPrice: parseFloat(dataValues[date]["1. open"]),
                    ClosePrice: parseFloat(dataValues[date]["4. close"]),
                    High: parseFloat(dataValues[date]["2. high"]),
                    Low: parseFloat(dataValues[date]["3. low"]),
                };
                listPoints.push(point);
            }
        }
    }
    catch (err) {
        console.log("Some problem occured while waiting for the data from the API " + JSON.stringify(err));
    }
    sendListStockData(listPoints);
}

//downloadStockDataThenSend();
downloadTextDataThenSend();

