"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendListTextData = exports.sendListStockData = exports.sendData = void 0;
var AWS = require("aws-sdk");
AWS.config.update({
    region: "us-east-1",
    endpoint: "https://dynamodb.us-east-1.amazonaws.com",
});
var documentClient = new AWS.DynamoDB.DocumentClient();
// The put params for both tables.
var putParamsStock = {
    TableName: "StockData",
};
var putParamsText = {
    TableName: "TextData",
};
// This function sends a data point into a specific database.
function sendData(item, putParams) {
    putParams["Item"] = item;
    documentClient.put(putParams, function (err, data) {
        if (err) {
            console.log("Some error occured here it is: " + JSON.stringify(err));
            console.log("The data attempted to be sent " + JSON.stringify(data));
        }
        else {
            console.log("added succesfully the data");
        }
    });
}
exports.sendData = sendData;
// This function sends a list of stock data.
function sendListStockData(arrayData) {
    for (var _i = 0, arrayData_1 = arrayData; _i < arrayData_1.length; _i++) {
        var point = arrayData_1[_i];
        sendData(point, putParamsStock);
    }
}
exports.sendListStockData = sendListStockData;
// This function sends a list of text data.
function sendListTextData(arrayData) {
    for (var _i = 0, arrayData_2 = arrayData; _i < arrayData_2.length; _i++) {
        var point = arrayData_2[_i];
        sendData(point, putParamsText);
    }
}
exports.sendListTextData = sendListTextData;
//# sourceMappingURL=Data_Sender.js.map