"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Data_Receiver_1 = require("./Data_Receiver");
var Data_Sender_1 = require("./Data_Sender");
// These are parameters that is used to query the webservices.
var STOCK_SYMBOLS = ["TSLA", "F", "NIO", "TM", "HMC"];
var COMPANY_NAMES = ["Tesla", "Ford", "Nio", "Toyota", "Honda"];
var WINDOW_SIZE = "60min";
var DATA_KEY = "Time Series (" + WINDOW_SIZE + ")";
// This function downloads and sends tweets to the database.
function downloadTextDataThenSend() {
    return __awaiter(this, void 0, void 0, function () {
        var promises, _i, COMPANY_NAMES_1, symbol, listPoints, values, index, name_1, dataValues, _a, dataValues_1, tweet, point, err_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    promises = [];
                    for (_i = 0, COMPANY_NAMES_1 = COMPANY_NAMES; _i < COMPANY_NAMES_1.length; _i++) {
                        symbol = COMPANY_NAMES_1[_i];
                        // We search for 250 tweets per company.
                        promises.push((0, Data_Receiver_1.textDataReceiver)(symbol, 250));
                    }
                    listPoints = [];
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, Promise.all(promises)];
                case 2:
                    values = _b.sent();
                    for (index = 0; index < values.length; index++) {
                        name_1 = STOCK_SYMBOLS[index];
                        dataValues = values[index]["statuses"];
                        // we create the texPoint to be sent to the table.
                        for (_a = 0, dataValues_1 = dataValues; _a < dataValues_1.length; _a++) {
                            tweet = dataValues_1[_a];
                            point = {
                                ID: tweet["id"],
                                Text: tweet["text"],
                                Company: name_1,
                                Timestamp: new Date(tweet["created_at"]).valueOf(),
                            };
                            // Populate the array of points.
                            listPoints.push(point);
                        }
                    }
                    return [3 /*break*/, 4];
                case 3:
                    err_1 = _b.sent();
                    console.log("Some problem occured while waiting for the data from the API " + JSON.stringify(err_1));
                    return [3 /*break*/, 4];
                case 4:
                    // Send the points to the database.
                    (0, Data_Sender_1.sendListTextData)(listPoints);
                    return [2 /*return*/];
            }
        });
    });
}
// This function downloads the stock data before sending it to the database.
// Works in the same way as the text downloading function.
function downloadStockDataThenSend() {
    return __awaiter(this, void 0, void 0, function () {
        var promises, _i, STOCK_SYMBOLS_1, symbol, listPoints, values, index, stockSymbol, dataValues, _a, _b, date, point, err_2;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    promises = [];
                    for (_i = 0, STOCK_SYMBOLS_1 = STOCK_SYMBOLS; _i < STOCK_SYMBOLS_1.length; _i++) {
                        symbol = STOCK_SYMBOLS_1[_i];
                        promises.push((0, Data_Receiver_1.stockDataReceiver)(symbol, WINDOW_SIZE));
                    }
                    listPoints = [];
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, Promise.all(promises)];
                case 2:
                    values = _c.sent();
                    for (index = 0; index < values.length; index++) {
                        stockSymbol = STOCK_SYMBOLS[index];
                        dataValues = values[index]["data"][DATA_KEY];
                        for (_a = 0, _b = Object.keys(dataValues); _a < _b.length; _a++) {
                            date = _b[_a];
                            point = {
                                Timestamp: new Date(date).valueOf(),
                                Symbol: stockSymbol,
                                OpenPrice: parseFloat(dataValues[date]["1. open"]),
                                ClosePrice: parseFloat(dataValues[date]["4. close"]),
                                High: parseFloat(dataValues[date]["2. high"]),
                                Low: parseFloat(dataValues[date]["3. low"]),
                            };
                            listPoints.push(point);
                        }
                    }
                    return [3 /*break*/, 4];
                case 3:
                    err_2 = _c.sent();
                    console.log("Some problem occured while waiting for the data from the API " + JSON.stringify(err_2));
                    return [3 /*break*/, 4];
                case 4:
                    (0, Data_Sender_1.sendListStockData)(listPoints);
                    return [2 /*return*/];
            }
        });
    });
}
//downloadStockDataThenSend();
downloadTextDataThenSend();
//# sourceMappingURL=Main.js.map