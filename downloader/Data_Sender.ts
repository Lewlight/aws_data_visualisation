import { DataPipeline } from "aws-sdk";
import { ObjectType } from "aws-sdk/clients/clouddirectory";
import { double } from "aws-sdk/clients/lightsail";
import { send } from "process";

// This interface represents a row for the stock table.
export interface dataPoint {
    Timestamp: number,
    Symbol: string,
    OpenPrice: number,
    ClosePrice: number,
    High: number,
    Low: number,
}

// This interface represents a row for the tweets table.
export interface textPoint {
    ID: number,
    Timestamp: number,
    Text: string,
    Company: string,
}

let AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "https://dynamodb.us-east-1.amazonaws.com",
});

let documentClient = new AWS.DynamoDB.DocumentClient();

// The put params for both tables.
const putParamsStock: object = {
    TableName: "StockData",
};

const putParamsText: object = {
    TableName: "TextData",
};

// This function sends a data point into a specific database.
export function sendData(item: object, putParams: Object) {
    putParams["Item"] = item;
    documentClient.put(putParams, (err, data) => {
        if (err) {
            console.log("Some error occured here it is: " + JSON.stringify(err));
            console.log("The data attempted to be sent " + JSON.stringify(data));
        }
        else {
            console.log("added succesfully the data");
        }
    });
}

// This function sends a list of stock data.
export function sendListStockData(arrayData: Array<dataPoint>) {
    for (let point of arrayData) {
        sendData(point, putParamsStock);
    }
}

// This function sends a list of text data.
export function sendListTextData(arrayData: Array<textPoint>) {
    for (let point of arrayData) {
        sendData(point, putParamsText);
    }
}