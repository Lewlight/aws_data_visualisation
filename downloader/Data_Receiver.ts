import { fdatasync } from "fs";

let dotenv = require("dotenv");
let axios = require("axios");
let twitter = require("twitter");

dotenv.config();

// The keys needed by the functions.
const ALPHA_API_KEY = process.env.API_KEY_ALPHA;
const TWITTER_ACCESS_TOKEN_KEY = process.env.ACCESS_TOKEN_KEY_TWITTER;
const TWITTER_ACCESS_SECRET_KEY = process.env.ACCESS_TOKEN_SECRET_TWITTER;
const TWITTER_CONSUMER_KEY = process.env.CONSUMER_KEY_TWITTER;
const TWITTER_CONSUMER_SECRET = process.env.CONSUMER_SECRET_TWITTER;

let client = new twitter({
    consumer_key: TWITTER_CONSUMER_KEY,
    consumer_secret: TWITTER_CONSUMER_SECRET,
    access_token_key: TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: TWITTER_ACCESS_SECRET_KEY,
});

// This function gets the result of the webservice for stock data.
export async function stockDataReceiver(symbol: string, windowSize: string): Promise<Object> {
    let baseUrl: string = "https://www.alphavantage.co/query?";
    baseUrl += 'function=TIME_SERIES_INTRADAY&outputsize=full&symbol=';
    baseUrl += symbol + "&interval=" + windowSize + "&apikey=" + ALPHA_API_KEY;
    return axios.get(baseUrl);
}

// This function returns a number of tweets given a query (the company name).
export async function textDataReceiver(query: string, tweetCount: number): Promise<Object> {
    try {
        let params: Object = {
            q: query,
            count: tweetCount,
            lang: "en",
        };
        let a = await client.get('search/tweets', params);
        console.log(a);
        return a;
    }
    catch (error) {
        console.log("Some error occured while trying to retireve tweets" + JSON.stringify(error));
    }
}
