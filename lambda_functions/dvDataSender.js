let AWS = require("aws-sdk");


const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    endpoint: "3704helh85.execute-api.us-east-1.amazonaws.com/production"
});
//Create new DocumentClient
let documentClient = new AWS.DynamoDB.DocumentClient();


// This function handles the requets to send data
exports.handler = async function (event, context) {
    console.log("starting send");
    try {
        // We get the ids.
        let clientIds = event.clientIds;
        // We create an array of promises that send the data from the event,
        // to every id.
        let senderPromises = clientIds.map(async id => {
            try {
                console.log("Sending some data to a client " + JSON.stringify(event));
                await sendDataToOne(event.data, id);
            }
            catch (err) {
                console.log("Some unresolved error occured while sending the data " + JSON.stringify(err));
                throw err;
            }
        });
        // Await for the promises and then succeed this lambda function.
        await Promise.all(senderPromises);
        context.succeed({
            state: true,
            message: "The data was sent back with no issues.",
            status: 200,
        });
    }
    catch (err) {
        // Add here proper context for when there is an error.
        console.log("An error occured waiting for all the promises to send data " + JSON.stringify(err) + err);
        context.fail({
            state: false,
            message: "An error occured while attempting to send data to client.",
            status: err.statusCode,
        });
    }

};

// This function sends the data to a specific id.
async function sendDataToOne(data, id) {
    // We stup the object used to send the data.
    let apiMsg = {
        ConnectionId: id,
        Data: data
    };
    console.log("Attempting to send " + JSON.stringify(apiMsg));
    try {
        //Wait for API Gateway to execute and log result
        await apigwManagementApi.postToConnection(apiMsg).promise();
        console.log("Message '" + data + "' sent to: " + id);
    }
    catch (err) {
        console.log("Failed to send message to: " + id);

        //Delete connection ID from database if the connection is not available.
        if (err.statusCode == 410) {
            try {
                console.log("Deleting the ID " + id);
                await deleteConnectionId(id);
            }
            catch (err) {
                console.log("ERROR deleting connectionId: " + JSON.stringify(err));
                throw err;
            }
        }
        else {
            console.log("UNKNOWN ERROR: " + JSON.stringify(err));
            throw err;
        }
    }
}

// This function deletes a connection id from the connection table.
async function deleteConnectionId(id) {
    let params = {
        TableName: "ConnectionsTable",
        Key: {
            ConnectionID: id
        }
    };

    return documentClient.delete(params).promise();
}
