let AWS = require("aws-sdk");

let documentClient = new AWS.DynamoDB.DocumentClient();
let lambda = new AWS.Lambda();

// This function handles updates in the database before sending it to
// connected clients.
exports.handler = async (event, context) => {
    let params = {
        TableName: "ConnectionsTable"
    };
    // We get the Ids for all the connected clients.
    let ids = await documentClient.scan(params).promise();
    // Usage of map to extract the id from the response.
    ids = ids["Items"].map(x => x["ConnectionID"]);
    console.log("IDS found " + JSON.stringify(ids));
    // This will be the object used to call the lambda function responsible
    // for sending data to clients.
    let sendBackData = {
        type: "update",
    };
    console.log("The event received " + JSON.stringify(event));
    for (let record of event.Records) {
        // If data was inserted we send it back.
        if (record.eventName === "INSERT") {
            if ("Symbol" in record.dynamodb.Keys) {
                // Depending on the table if it is predicted table or actual stock table
                // We use typeData to create the object to return to the client.
                let typeData = "stockData";
                if (record.eventSourceARN.includes("PredictedStockData")) {
                    typeData = "predictedData";
                };
                console.log("Table of data received " + typeData);
                // Extract all the information from the update.
                let company = record.dynamodb.Keys.Symbol.S;
                // If the company hasn't been added yet to the object we add it.
                if (!(company in sendBackData)) {
                    sendBackData[company] = {};
                }
                if (!(typeData in sendBackData[company])) {
                    sendBackData[company][typeData] = [];
                }
                // We add the object in the same format when querying the table
                // This allows the method in the client side to handle it correctly.
                sendBackData[company][typeData].push({
                    Symbol: record.dynamodb.Keys.Symbol.S,
                    Timestamp: record.dynamodb.Keys.Timestamp.N,
                    OpenPrice: record.dynamodb.NewImage.OpenPrice.N,
                    ClosePrice: record.dynamodb.NewImage.ClosePrice.N,
                    High: record.dynamodb.NewImage.High.N,
                    Low: record.dynamodb.NewImage.Low.N,
                });

            }
            else {
                // We follow the same logic for sentiment data.
                let company = record.dynamodb.NewImage.Company.S;
                if (!(company in sendBackData)) {
                    sendBackData[company] = {};
                }
                if (!("sentimentData" in sendBackData[company])) {
                    sendBackData[company]["sentimentData"] = [];
                }
                console.log("The event: " + console.log(JSON.stringify(event)));
                console.log("The record " + console.log(JSON.stringify(record)));
                // format the object in the same way as the response of querying 
                // the sentiment data table.
                sendBackData[company]["sentimentData"].push({
                    Timestamp: record.dynamodb.Keys.Timestamp.N,
                    Company: record.dynamodb.NewImage.Company.S,
                    Sentiment: {
                        Sentiment: record.dynamodb.NewImage.Sentiment.M.Sentiment.S,
                        SentimentScore: {
                            Neutral: record.dynamodb.NewImage.Sentiment.M.SentimentScore.M.Neutral.N,
                            Mixed: record.dynamodb.NewImage.Sentiment.M.SentimentScore.M.Mixed.N,
                            Positive: record.dynamodb.NewImage.Sentiment.M.SentimentScore.M.Positive.N,
                            Negative: record.dynamodb.NewImage.Sentiment.M.SentimentScore.M.Negative.N,
                        }
                    },
                    ID: record.dynamodb.Keys.ID.N,
                });
            }
        }
    }
    console.log("The data we send back " + JSON.stringify(sendBackData));
    let sendBack = {
        clientIds: ids,
        data: JSON.stringify(sendBackData),
    };
    // Once the data has been organised we invoke the lambda function
    // responsible for sending the data.
    try {
        let lambdaParams = {
            FunctionName: "dvDataSender",
            InvocationType: "RequestResponse",
            LogType: "Tail",
            Payload: JSON.stringify(sendBack),
        };
        // Await the response of the lambda function
        let response = await lambda.invoke(lambdaParams).promise();
        let payload = response.Payload;
        console.log("payload " + payload);
        payload = JSON.parse(payload);
        // Depending on the response we return a different status code.
        console.log("The mssg" + JSON.stringify(response));
        console.log("" + payload.state + " " + payload.message);
        if (payload.state) {
            return {
                statusCode: 200,
                body: payload.message
            };
        }
        else {
            return {
                statusCode: 500,
                body: payload.message
            };
        }
    }
    catch (err) {
        console.log("an error occured in db " + JSON.stringify(err));
    }
};