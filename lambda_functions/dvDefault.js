exports.handler = async (event) => {
    console.log("some event:" + JSON.stringify(event));

    //Build and return response with error message
    const response = {
        statusCode: 500,
        body: JSON.stringify('A message is not recognised received from API Gateway.'),
    };
    return response;
};