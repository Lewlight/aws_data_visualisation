
let AWS = require("aws-sdk");

// the parameters used by comprehend.
let params = {
    LanguageCode: "en",
}

let putParams = {
    TableName: "TextSentimentAnalysis",
}

let documentClient = new AWS.DynamoDB.DocumentClient();
let comprehend = new AWS.Comprehend();

// This function handles addition to the table of TextData by performing sentiment analysis.
exports.handler = async (event) => {
    for (let record of event.Records) {
        if (record.eventName === "INSERT") {
            console.log("The text we analyse is: " + record.dynamodb.NewImage.Text.S);
            // We get the text.
            params.Text = record.dynamodb.NewImage.Text.S;
            try {
                // We use comprehend to perform the sentiment analysis on the text.
                let result = await comprehend.detectSentiment(params).promise();
                console.log("Sending in some data to the database " + JSON.stringify(result));

                // We set up the object to put the results in the table.
                putParams["Item"] = {
                    ID: parseInt(record.dynamodb.NewImage.ID.N),
                    Timestamp: parseInt(record.dynamodb.NewImage.Timestamp.N),
                    Company: record.dynamodb.NewImage.Company.S,
                    Sentiment: result,
                }
                
                // Add the setinment analysis into the table.
                await documentClient.put(putParams).promise();
            }
            catch (error) {
                console.log("An error occured when trying to perform sentiment analysis " + console.log(JSON.stringify(error)));
            }
        }
    }
}
