let AWS = require("aws-sdk");

let documentClient = new AWS.DynamoDB.DocumentClient();
let lambda = new AWS.Lambda();

// This function handles request of data from the client side.
exports.handler = async (event, context) => {
    console.log("inside provider, we will get " + JSON.parse(event.body).data);
    let company = JSON.parse(event.body).data;
    // We get the id of the client who made the request.
    let id = event.requestContext.connectionId;

    // We request the latest 100 points from stock data, sentiment data
    // and predicted data.
    let stockQueryParam = {
        Limit: 100,
        TableName: "StockData",
        ExpressionAttributeValues: {
            ":symb": company,
        },
        KeyConditionExpression: "Symbol = :symb",
        ScanIndexForward: false,
    };
    let predictedStockQueryParam = {
        Limit: 100,
        TableName: "PredictedStockData",
        ExpressionAttributeValues: {
            ":symb": company,
        },
        KeyConditionExpression: "Symbol = :symb",
        ScanIndexForward: false,
    };
    let sentimentQueryParam = {
        Limit: 100,
        TableName: "TextSentimentAnalysis",
        ExpressionAttributeValues: {
            ":symb": company,
        },
        IndexName: "Company-Timestamp-index",
        KeyConditionExpression: "Company = :symb",
        ScanIndexForward: false,
    }
    try {
        // Query the database for the data.
        let stockQuery = await documentClient.query(stockQueryParam).promise();
        // We reverse the items to return them in order.
        let historicalData = stockQuery.Items.reverse();
        let predictedStockQuery = await documentClient.query(predictedStockQueryParam).promise();
        let predictedData = predictedStockQuery.Items.reverse();
        let sentimentQuery = await documentClient.query(sentimentQueryParam).promise();
        let sentimentData = sentimentQuery.Items.reverse();
        let sendBackData = {};
        // We setup the object used by the lambda function that sends data.
        sendBackData[company] = {
            "type": "requestResponse",
            "sentimentData": sentimentData,
            "stockData": historicalData,
            "predictedData": predictedData,
        };
        let sendBack = {
            clientIds: [id],
            data: JSON.stringify(sendBackData)
        };
        console.log("object for send back" + JSON.stringify(sendBack));
        let lambdaParams = {
            FunctionName: "dvDataSender",
            InvocationType: "RequestResponse",
            LogType: "Tail",
            Payload: JSON.stringify(sendBack),
        };
        // We invoke the lambda function with the payload.
        let response = await lambda.invoke(lambdaParams).promise();
        let payload = response.Payload;
        console.log("payload " + payload);
        payload = JSON.parse(payload);
        // Depending on the response we return a different status code
        console.log("The mssg" + JSON.stringify(response));
        console.log("" + payload.state + " " + payload.message);
        if (payload.state) {
            return {
                statusCode: 200,
                body: payload.message
            };
        }
        else {
            return {
                statusCode: 500,
                body: payload.message
            };
        }
    }
    catch (err) {
        console.log("an error occured in db " + JSON.stringify(err));
        return {
            statusCode: err.status,
            body: err.message
        };
    }
};
