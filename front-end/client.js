
let connection = new WebSocket("wss://3704helh85.execute-api.us-east-1.amazonaws.com/production");

// We store here the data used to display on the website.
let localData = {
    displayed: "TSLA",
    TSLA: {
        stockData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
        sentimentData: {
            positive: [],
            negative: [],
            neutral: [],
            mixed: [],
            time: [],
        },
        predictedData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
    },
    NIO: {
        stockData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
        sentimentData: {
            positive: [],
            negative: [],
            neutral: [],
            mixed: [],
            time: [],
        },
        predictedData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
    },
    F: {
        stockData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
        sentimentData: {
            positive: [],
            negative: [],
            neutral: [],
            mixed: [],
            time: [],
        },
        predictedData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
    },
    HMC: {
        stockData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
        sentimentData: {
            positive: [],
            negative: [],
            neutral: [],
            mixed: [],
            time: [],
        },
        predictedData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
    },
    TM: {
        stockData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
        sentimentData: {
            positive: [],
            negative: [],
            neutral: [],
            mixed: [],
            time: [],
        },
        predictedData: {
            open: [],
            close: [],
            high: [],
            low: [],
            time: [],
        },
    },
};

//Log connected response, and call for initial data to be displayed.
connection.onopen = function (event) {
    console.log("Connected: " + JSON.stringify(event));
    requestData("TSLA");
};

//Manage the data sent from the server.
connection.onmessage = function (msg) {
    console.log("Data received from server." + msg.data);
    data = JSON.parse(msg.data)
    // We first update the localData.
    for (let company of Object.keys(data)) {
        console.log("updating data for " + company + JSON.stringify(data[company]));
        updateData(company, data[company]);
    }
    // We then update the plot.
    updatePlot();
}

//Log errors
connection.onerror = function (error) {
    console.log("WebSocket Error: " + JSON.stringify(error));
    document.getElementById("error").innerHTML = "Some error occured in the server.";
}


//This function requests the initial data for a certain company.
function requestData(company) {
    // We change the clors of the buttons to highlight the right company.
    let button = document.getElementById(localData.displayed + "_button");
    button.style["background-color"] = "lightcoral";
    localData.displayed = company;
    button = document.getElementById(company + "_button");
    button.style["background-color"] = "lightgreen";
    // If the data for a company has been initialised (already requested).
    // We don't request it again, as it is locally stored, and any updates
    // in the server is automatically reflected in localData.
    if (localData[company].initialised) {
        console.log("Displaying locally stored data for " + company);
        updatePlot();
        return;
    }
    //Create request JSON
    let msgObject = {
        action: "requestData",//Used for routing in API Gateway
        data: company
    };
    // We assume that is has been initialised.
    localData[company]["initialised"] = true;
    //Send request
    connection.send(JSON.stringify(msgObject));

    //Log message to server.
    console.log("Request sent: " + JSON.stringify(msgObject));
}

// This method converts a timestamp to a usable label for the plot.
function convertToDate(timestamp) {
    let dateData = new Date(timestamp);
    dateData = dateData.toString().slice(0, 10) + dateData.toString().slice(15, 21);
    return dateData;
}

// This method updates local data for a specific company.
function updateData(company, data) {
    // If the data from the server is stockData we update it.
    if (data.hasOwnProperty('stockData')) {
        let stockData = data["stockData"];
        for (let dataPoint of stockData) {
            // we push the new data to the arrays in the localData.
            localData[company].stockData.open.push(dataPoint["OpenPrice"]);
            localData[company].stockData.close.push(dataPoint["ClosePrice"]);
            localData[company].stockData.high.push(dataPoint["High"]);
            localData[company].stockData.low.push(dataPoint["Low"]);
            let dateData = convertToDate(dataPoint.Timestamp);
            localData[company].stockData.time.push(dateData);
        }
        // We then cut the localData to keep it at a fixed size we use in the display.
        if (localData[company].stockData.open.length > 100) {
            sliceIndex = localData[company].stockData.open.length - 100;
            localData[company].stockData.open = localData[company].stockData.open.slice(sliceIndex);
            localData[company].stockData.close = localData[company].stockData.close.slice(sliceIndex);
            localData[company].stockData.high = localData[company].stockData.high.slice(sliceIndex);
            localData[company].stockData.low = localData[company].stockData.low.slice(sliceIndex);
            localData[company].stockData.time = localData[company].stockData.time.slice(sliceIndex);
        }
    }
    // The same logic is applied to the other 2 types of data.
    if (data.hasOwnProperty('predictedData')) {
        let stockData = data["predictedData"];
        for (let dataPoint of stockData) {
            localData[company].predictedData.open.push(dataPoint["OpenPrice"]);
            localData[company].predictedData.close.push(dataPoint["ClosePrice"]);
            localData[company].predictedData.high.push(dataPoint["High"]);
            localData[company].predictedData.low.push(dataPoint["Low"]);
            let dateData = convertToDate(dataPoint.Timestamp);
            localData[company].predictedData.time.push(dateData);
        }
        if (localData[company].predictedData.open.length > 100) {
            sliceIndex = localData[company].predictedData.open.length - 100;
            localData[company].predictedData.open = localData[company].predictedData.open.slice(sliceIndex);
            localData[company].predictedData.close = localData[company].predictedData.close.slice(sliceIndex);
            localData[company].predictedData.high = localData[company].predictedData.high.slice(sliceIndex);
            localData[company].predictedData.low = localData[company].predictedData.low.slice(sliceIndex);
            localData[company].predictedData.time = localData[company].predictedData.time.slice(sliceIndex);
        }
    }

    if (data.hasOwnProperty('sentimentData')) {
        let sentimentData = data["sentimentData"];
        for (let dataPoint of sentimentData) {
            localData[company].sentimentData.positive.push(dataPoint.Sentiment.SentimentScore.Positive);
            localData[company].sentimentData.negative.push(dataPoint.Sentiment.SentimentScore.Negative);
            localData[company].sentimentData.neutral.push(dataPoint.Sentiment.SentimentScore.Neutral);
            localData[company].sentimentData.mixed.push(dataPoint.Sentiment.SentimentScore.Mixed);
            localData[company].sentimentData.time.push(dataPoint.Timestamp);
        }
        if (localData[company].sentimentData.positive.length > 100) {
            sliceIndex = localData[company].sentimentData.positive.length - 100;
            localData[company].sentimentData.positive = localData[company].sentimentData.positive.slice(sliceIndex);
            localData[company].sentimentData.negative = localData[company].sentimentData.negative.slice(sliceIndex);
            localData[company].sentimentData.neutral = localData[company].sentimentData.neutral.slice(sliceIndex);
            localData[company].sentimentData.mixed = localData[company].sentimentData.mixed.slice(sliceIndex);
            localData[company].sentimentData.time = localData[company].sentimentData.time.slice(sliceIndex);
        }
    }
}

// This method updates the plots on the page.
function updatePlot() {
    let plotDiv = document.getElementById("sentimentPlot");
    let sentimentTraces = [];
    // We get the company that needs to be displayed.
    let company = localData.displayed;
    // We add the 4 traces for the sentiment data.
    sentimentTraces.push({ y: localData[company].sentimentData.positive, x: localData[company].sentimentData.time, line: { color: 'green' }, stackgroup: 'one', name: company + " positive." });
    sentimentTraces.push({ y: localData[company].sentimentData.negative, x: localData[company].sentimentData.time, line: { color: 'red' }, stackgroup: 'one', name: company + " negative." });
    sentimentTraces.push({ y: localData[company].sentimentData.mixed, x: localData[company].sentimentData.time, line: { color: 'orange' }, stackgroup: 'one', name: company + " mixed." });
    sentimentTraces.push({ y: localData[company].sentimentData.neutral, x: localData[company].sentimentData.time, line: { color: 'lightblue' }, stackgroup: 'one', name: company + " neutral." });
    Plotly.newPlot(plotDiv, sentimentTraces, { title: 'Sentiment Analysis of recent tweets.' });


    let otherPlotDiv = document.getElementById("stockPlot");
    let stockTraces = [];

    // We add the stock trace and predictions trace.
    stockTraces.push({
        x: localData[company].stockData.time,
        close: localData[company].stockData.close,
        open: localData[company].stockData.open,
        high: localData[company].stockData.high,
        low: localData[company].stockData.low,
        type: 'candlestick',
        xaxis: 'x',
        yaxis: 'y',
        name: company + " stock data.",
        decreasing: { line: { color: '#7F7F7F' } },
        increasing: { line: { color: '#17BECF' } },
    });
    stockTraces.push({
        x: localData[company].predictedData.time,
        close: localData[company].predictedData.close,
        open: localData[company].predictedData.open,
        high: localData[company].predictedData.high,
        low: localData[company].predictedData.low,
        type: 'candlestick',
        xaxis: 'x',
        yaxis: 'y',
        name: company + " predicted data.",
        decreasing: { line: { color: 'red' } },
        increasing: { line: { color: 'green' } },
    });


    let layout = {
        title: 'Predicted and historical stock data.',
        dragmode: 'zoom',
        showlegend: true,
        xaxis: {
            rangeslider: {
                visible: false
            },
            automargin: true
        },
        yaxis: {
            automargin: true
        }
    };
    Plotly.newPlot(otherPlotDiv, stockTraces, layout);
}